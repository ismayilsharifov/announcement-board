package com.announcement.board.mapper;

import com.announcement.board.dto.request.AnnouncementDto;
import com.announcement.board.dto.request.AnnouncementRequestDto;
import com.announcement.board.dto.response.AnnouncementResponseDto;
import com.announcement.board.dao.entity.Announcement;
import com.announcement.board.dao.entity.Section;
import com.announcement.board.dto.request.SectionRequestDto;
import com.announcement.board.dto.response.SectionResponseDto;
import org.mapstruct.Mapper;

@Mapper
public interface BoardMapper {


    AnnouncementResponseDto mapAnnouncementResponse(Announcement announcement);

    Announcement mapAnnouncementRequest(AnnouncementRequestDto announcementRequestDto);

    SectionResponseDto mapSectionResponse(Section section);

    Section mapSectionEntity(SectionRequestDto sectionRequestDto);
}
