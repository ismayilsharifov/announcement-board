package com.announcement.board.controller;

import com.announcement.board.dto.request.AnnouncementRequestDto;
import com.announcement.board.dto.request.SectionRequestDto;
import com.announcement.board.dto.response.AnnouncementResponseDto;
import com.announcement.board.dto.response.SectionResponseDto;
import com.announcement.board.service.BoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@Secured("ROLE_USER")
@RestController
@RequestMapping("/api/board")
@RequiredArgsConstructor
public class BoardController {

    private final BoardService boardService;

    @GetMapping("/announcements")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<List<AnnouncementResponseDto>> getAnnouncementList(@PathVariable String sectionId) {
        return ResponseEntity.status(HttpStatus.OK).body(boardService.getAnnouncementList(sectionId));
    }

    @GetMapping("/announcements/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<AnnouncementResponseDto> getAnnouncementById(@PathVariable String id){
        return ResponseEntity.ok().body(boardService.getAnnouncementById(id));
    }


    @PostMapping("/announcements")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<AnnouncementResponseDto> createAnnouncement(
            @RequestBody AnnouncementRequestDto announcementRequestDto) {
        return ResponseEntity.ok().body(boardService.createAnnouncement(announcementRequestDto));
    }

    @PutMapping("/announcements/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<AnnouncementResponseDto> updateAnnouncement(@PathVariable String id,
                                                                 @RequestBody AnnouncementRequestDto announcementRequestDto){
        return ResponseEntity.ok().body(boardService.updateAnnouncementById(id,announcementRequestDto));
    }

    @DeleteMapping("/announcements/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<String> deleteAnnouncement(@PathVariable String id){
        boardService.deleteAnnouncement(id);
        return new ResponseEntity<>(id, HttpStatus.ACCEPTED);
    }

    @GetMapping("/sections")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<List<SectionResponseDto>> getSectionList() {
        return ResponseEntity.status(HttpStatus.OK).body(boardService.getSectionList());
    }

    @GetMapping("/sections/{id}")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public ResponseEntity<SectionResponseDto> getSectionById(@PathVariable String id){
        return ResponseEntity.ok().body(boardService.getSectionById(id));
    }


    @PostMapping("/sections")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<SectionResponseDto> createSection(@RequestBody SectionRequestDto sectionRequestDto) {
        return ResponseEntity.ok().body(boardService.createSection(sectionRequestDto));
    }

    @PutMapping("/sections/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<SectionResponseDto> updateSection(@PathVariable String id,
                                                            @RequestBody SectionRequestDto requestDto){
        return ResponseEntity.ok().body(boardService.updateSectionById(id,requestDto));
    }

    @DeleteMapping("/sections/{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public ResponseEntity<String> deleteSection(@PathVariable String id){
        boardService.deleteSection(id);
        return new ResponseEntity<>(id, HttpStatus.ACCEPTED);
    }
}
