package com.announcement.board.service;

import com.announcement.board.dto.request.AnnouncementRequestDto;
import com.announcement.board.dto.request.SectionRequestDto;
import com.announcement.board.dto.response.AnnouncementResponseDto;
import com.announcement.board.dto.response.SectionResponseDto;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BoardService {

    AnnouncementResponseDto getAnnouncementById(String id);

    List<AnnouncementResponseDto> getAnnouncementList(String sectionId);

    AnnouncementResponseDto createAnnouncement( AnnouncementRequestDto announcementRequestDto);


    AnnouncementResponseDto updateAnnouncementById(String id, AnnouncementRequestDto announcementRequestDto);

    void deleteAnnouncement(String id);

    SectionResponseDto getSectionById(String id);

    List<SectionResponseDto> getSectionList();

    SectionResponseDto createSection(SectionRequestDto sectionRequestDto);

    SectionResponseDto updateSectionById(String id, SectionRequestDto sectionRequestDto);

    void deleteSection(String id);


}
