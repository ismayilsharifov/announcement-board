package com.announcement.board.service;

import com.announcement.board.dao.repository.AnnouncementRepository;
import com.announcement.board.dao.repository.SectionRepository;
import com.announcement.board.dto.request.AnnouncementRequestDto;
import com.announcement.board.dto.request.SectionRequestDto;
import com.announcement.board.dto.response.AnnouncementResponseDto;
import com.announcement.board.dto.response.SectionResponseDto;
import com.announcement.board.mapper.BoardMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BoardServiceImpl implements BoardService {

    private final AnnouncementRepository announcementRepository;

    private final SectionRepository sectionRepository;

    private final BoardMapper boardMapper;

    @Override
    public AnnouncementResponseDto getAnnouncementById(String id) {
        var announcement = announcementRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Announcement not found with parameter {" + id + "}"));
        return boardMapper.mapAnnouncementResponse(announcement);
    }

    @Override
    public List<AnnouncementResponseDto> getAnnouncementList(String sectionId) {
        var section = sectionRepository.findById(sectionId).orElseThrow(() ->
                new EntityNotFoundException("Section not found with parameter {" + sectionId + "}"));
        return announcementRepository.findAllBySection(section).stream().map(
                        boardMapper::mapAnnouncementResponse)
                .collect(Collectors.toList());
    }

    @Override
    public AnnouncementResponseDto createAnnouncement(AnnouncementRequestDto announcementRequestDto) {
        var section = sectionRepository.findById(announcementRequestDto.getSectionId())
                .orElseThrow(() -> new EntityNotFoundException("Section not found with parameter {"
                        + announcementRequestDto.getSectionId() + "}"));
        var announcement = boardMapper.mapAnnouncementRequest(announcementRequestDto);
        announcement.setSection(section);
        announcement = announcementRepository.save(announcement);
        return boardMapper.mapAnnouncementResponse(announcement);
    }

    @Override
    public AnnouncementResponseDto updateAnnouncementById(String id, AnnouncementRequestDto announcementRequestDto) {
        var announcement = announcementRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Announcement not found with parameter {" + id + "}"));
        announcement = boardMapper.mapAnnouncementRequest(announcementRequestDto);
        var section = sectionRepository.findById(announcementRequestDto.getSectionId())
                .orElseThrow(() -> new EntityNotFoundException("Section not found with parameter {"
                        + announcementRequestDto.getSectionId() + "}"));
        announcement.setId(id);
        announcement.setSection(section);
        return boardMapper.mapAnnouncementResponse(announcementRepository.save(announcement));
    }

    @Override
    public void deleteAnnouncement(String id) {
        announcementRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Announcement not found with parameter {" + id + "}"));
        announcementRepository.deleteById(id);

    }

    @Override
    public SectionResponseDto getSectionById(String id) {
        var section = sectionRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Section not found with parameter {" + id + "}"));
        return boardMapper.mapSectionResponse(section);
    }

    @Override
    public List<SectionResponseDto> getSectionList() {
        return sectionRepository.findAll().stream().map(
                        boardMapper::mapSectionResponse)
                .collect(Collectors.toList());
    }

    @Override
    public SectionResponseDto createSection(SectionRequestDto sectionRequestDto) {
        var section = sectionRepository.save(boardMapper.mapSectionEntity(sectionRequestDto));
        return boardMapper.mapSectionResponse(section);
    }

    @Override
    public SectionResponseDto updateSectionById(String id, SectionRequestDto sectionRequestDto) {
        var section = sectionRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Section not found with parameter {" + id + "}"));
        section = boardMapper.mapSectionEntity(sectionRequestDto);
        section.setId(id);
        return boardMapper.mapSectionResponse(sectionRepository.save(section));
    }

    @Override
    public void deleteSection(String id) {
        var section = sectionRepository.findById(id).orElseThrow(() ->
                new EntityNotFoundException("Section not found with parameter {" + id + "}"));
        sectionRepository.deleteById(id);
        announcementRepository.findAllBySection(section)
                .forEach(announcement -> announcementRepository.deleteAnnouncementBySectionId(id));
    }
}
