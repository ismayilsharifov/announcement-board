package com.announcement.board.dao.repository;

import com.announcement.board.dao.entity.Section;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SectionRepository extends JpaRepository<Section, String> {
}
