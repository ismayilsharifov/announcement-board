package com.announcement.board.dao.repository;

import com.announcement.board.dao.entity.Announcement;
import com.announcement.board.dao.entity.Section;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AnnouncementRepository extends JpaRepository<Announcement, String> {

    List<Announcement> findAllBySection(Section section);

    void deleteAnnouncementBySectionId(String sectionId);
}
