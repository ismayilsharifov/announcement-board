package com.announcement.board.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnnouncementRequestDto {

    @NotBlank(message = "Announcement title required!")
    @Size(min = 3, max = 200, message = "Title need to have between 10 and 200 characters!")
    private String title;

    @NotBlank(message = "Announcement content required!")
    @Size(min = 3, max = 4000, message = "Content need to have max 4000 characters!")
    private String content;

    @NotBlank(message = "Section required!")
    private String sectionId;
}
