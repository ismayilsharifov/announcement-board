package com.announcement.board.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SectionRequestDto {

    @NotBlank(message = "Section title required.")
    @Size(min = 3, max = 200, message = "title need to have between 10 and 200 characters")
    private String title;

    @Size(min = 3, max = 4000, message = "content need to have max 4000 characters")
    private String description;

}
