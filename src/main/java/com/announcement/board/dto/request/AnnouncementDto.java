package com.announcement.board.dto.request;

import com.announcement.board.dao.entity.Section;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnnouncementDto {

    private String title;
    private String content;
    private Section section;
}
