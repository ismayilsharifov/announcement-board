# Announcement Board With Sections

**Description API::**

- Sign-up Register User (Registration with JWT and HttpOnly Cookie).

- Sign-in Authenticate User (Security Login with JWT).

- Sign-out.

- Fetch one section.

- Fetch all sections.

- Create section 

- Update section

- Delete section

- Fetch one announcement.

- Fetch all announcement.

- Create announcement 

- Update announcement

- Delete announcement



**Features:**


- A client can signup, login, logout. 

- User can view all details about sections and announcements but have not access to modify.

- Admin can modify and delete sections and announcements.


**Backlog**


- In memory database (H2).

- Object relational mapping with JPA.

- Mapping data transfer object and domain classes with Mapstruct.

- Implement Spring Security to work with JWT.

- Custom exception handler.

